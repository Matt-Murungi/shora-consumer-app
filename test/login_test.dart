// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:shora_consumer/core/login/login.dart';
import 'package:shora_consumer/widgets/input.dart';
// import 'package:test/test.dart';

void main() {
  Widget makeTestableWidget({Widget child}) {
    return MaterialApp(
      home: child,
    );
  }

  testWidgets('login input is not empty', (WidgetTester tester) async {
    LoginScreen login = LoginScreen();
    await tester.pumpWidget(makeTestableWidget(child: login));

    Finder emailField = find.byWidget(Input(
        hint: "hint",
        inputType: TextInputType.emailAddress,
        inputKey: "email"));
    await tester.enterText(emailField, 'email');

    // assert()
    // await tester.tap(find.byKey(Key('signIn')));

  });
}
