import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shora_consumer/utils/logo.dart';
import 'package:shora_consumer/widgets/CircleButton.dart';
import 'package:shora_consumer/widgets/heading.dart';
import 'package:shora_consumer/widgets/input.dart';
import 'package:shora_consumer/widgets/subheading.dart';

import '../../styles.dart';

class ShoppingLocation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          height: MediaQuery.of(context).size.height,
          margin: EdgeInsets.symmetric(horizontal: 28),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  height: 60,
                ),
                logoSvg,
                HeadingText(
                  heading: "Where do you want to shop from?",
                ),
                SubHeadingText(
                  subheading: "Enter a city you want to shop from",
                  color: kSubheadingPrimary,
                  align: TextAlign.center,
                ),
                SizedBox(
                  height: 32,
                ),
                Input(
                  hint: "Location",
                  inputKey: "location",
                  inputType: TextInputType.text,
                  trailWidget: Icon(Icons.my_location),
                ),
                SizedBox(
                  height: 30,
                ),
                CircleButton(
                  onPressed: () => Get.toNamed('/home'),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
