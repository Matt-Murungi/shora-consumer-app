import 'package:flutter/material.dart';
import 'package:shora_consumer/data/category_data.dart';
import 'package:shora_consumer/data/product_data.dart';
import 'package:shora_consumer/widgets/home_appbar.dart';
import 'package:shora_consumer/widgets/image_category.dart';
import 'package:shora_consumer/widgets/product_item.dart';

class HomeScreen extends StatelessWidget {
  List categoryList = categories;
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    // double width = MediaQuery.of(context).size.width;

    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            HomeAppBar(),
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 10,
              ),
              child: Container(
                height: height / 1.5,
                child: SingleChildScrollView(
                  child: Container(
                    child: Column(
                      children: [
                        SizedBox(
                          height: 24,
                        ),
                        RowTitle(
                          title: "Top Category",
                          buttonTitle: "See All",
                        ),
                        Container(
                          height: height / 3,
                          child: GridView.builder(
                              itemCount: categoryList.length,
                              physics: NeverScrollableScrollPhysics(),
                              gridDelegate:
                                  SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 3,
                                crossAxisSpacing: 3,
                                mainAxisSpacing: 2,
                              ),
                              itemBuilder: (context, index) => ImageCategory(
                                    image: categoryList[index].image,
                                    label: categoryList[index].label,
                                  )),
                        ),
                        RowTitle(
                          title: "Featured Products",
                          buttonTitle: "Filter by",
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Container(
                          height: height,
                          child: ListView.builder(
                              physics: NeverScrollableScrollPhysics(),
                              itemBuilder: (context, index) => ProductItem(
                                    productTitle: products[index].title,
                                    productDescription:
                                        products[index].description,
                                    productOldPrice: products[index].oldPrice,
                                    productPrice: products[index].newPrice,
                                    productImageUrl: products[index].imageUrl,
                                  ),
                              itemCount: products.length),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
