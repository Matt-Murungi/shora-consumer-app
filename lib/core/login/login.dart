import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shora_consumer/core/register/register.dart';
import 'package:shora_consumer/core/shopping_location/shopping_location.dart';
import 'package:shora_consumer/styles.dart';
import 'package:shora_consumer/utils/logo.dart';
import 'package:shora_consumer/widgets/TappableText.dart';
import 'package:shora_consumer/widgets/heading.dart';
import 'package:shora_consumer/widgets/input.dart';
import 'package:shora_consumer/widgets/primary_button.dart';
import 'package:shora_consumer/widgets/subheading.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    TextEditingController emailController = TextEditingController();
    TextEditingController passwordController = TextEditingController();

    bool isValidated(TextEditingController controller) {
      if (controller.value == null) {
        return null;
      }
      return true; 
      }

    return SafeArea(
      child: Scaffold(
          body: Container(
        height: MediaQuery.of(context).size.height,
        margin: EdgeInsets.symmetric(horizontal: 28),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              logoSvg,
              HeadingText(
                heading: "Welcome to Shora",
              ),
              SubHeadingText(
                subheading:
                    "Login and find 1,000's of products from you and your loved ones from different stores",
                color: kSubheadingPrimary,
                align: TextAlign.center,
              ),
              SizedBox(
                height: 32,
              ),
              Input(
                hint: "Email",
                inputKey: "email",
                inputType: TextInputType.emailAddress,
                controller: emailController,
              ),
              Input(
                hint: "Password",
                inputKey: "password",
                inputType: TextInputType.visiblePassword,
                trailWidget: Icon(Icons.visibility),
                controller: passwordController,
              ),
              TappableText(
                text: "Forgot my password!",
                onPressed: () {},
              ),
              SizedBox(
                height: 32,
              ),
              PrimaryButton(
                  label: "Login",
                  color: kPrimaryColor,
                  function: () => Get.to(ShoppingLocation())),
              SizedBox(
                height: 52,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SubHeadingText(
                    subheading: "I don’t have an account! ",
                  ),
                  TappableText(
                      text: "Register",
                      onPressed: () => Get.toNamed("/register"))
                ],
              ),
              SizedBox(
                height: 62,
              ),
              SubHeadingText(
                subheading: "By continuing, you agree to our ",
                align: TextAlign.center,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SubHeadingText(
                    subheading: "Terms of Service",
                    color: kPrimaryColor,
                    fontWeight: kBoldSubHeading,
                  ),
                  SubHeadingText(
                    subheading: " & ",
                    fontWeight: kBoldSubHeading,
                  ),
                  SubHeadingText(
                    subheading: "Privacy",
                    color: kPrimaryColor,
                    fontWeight: kBoldSubHeading,
                  )
                ],
              )
            ],
          ),
        ),
      )),
    );
  }
}
