import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';
import 'package:shora_consumer/utils/logo.dart';
import 'package:shora_consumer/widgets/TappableText.dart';
import 'package:shora_consumer/widgets/heading.dart';
import 'package:shora_consumer/widgets/subheading.dart';

import 'package:get/get.dart';
import 'package:shora_consumer/core/shopping_location/shopping_location.dart';
import 'package:shora_consumer/styles.dart';

import 'package:shora_consumer/widgets/input.dart';
import 'package:shora_consumer/widgets/primary_button.dart';

class RegisterScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          body: Container(
        height: MediaQuery.of(context).size.height,
        margin: EdgeInsets.symmetric(horizontal: 28),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              logoSvg,
              HeadingText(
                heading: "Register to create an account",
              ),
              SizedBox(
                height: 32,
              ),
              Input(
                hint: "First Name",
                inputKey: "firstName",
                inputType: TextInputType.text,
              ),
              Input(
                hint: "Last Name",
                inputKey: "lastName",
                inputType: TextInputType.text,
              ),
              Input(
                hint: "Email",
                inputKey: "email",
                inputType: TextInputType.emailAddress,
              ),
              Input(
                hint: "Phone Number (Optional)",
                inputKey: "phoneNumber",
                inputType: TextInputType.phone,
                leadWidget: CountryCodePicker(
                  initialSelection: 'BI',
                  onChanged: (code) {},
                  textStyle: kHintTextStyle,
                ),
              ),
              Input(
                hint: "Create Password",
                inputKey: "createPassword",
                inputType: TextInputType.visiblePassword,
                trailWidget: Icon(Icons.visibility),
              ),
              SizedBox(
                height: 32,
              ),
              PrimaryButton(
                  label: "Register",
                  color: kPrimaryColor,
                  function: () => Get.to(ShoppingLocation())),
              SizedBox(
                height: 52,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SubHeadingText(
                    subheading: "I already have an account! ",
                  ),
                  TappableText(
                    text: "Login",
                    onPressed: () => Get.toNamed("/login"),
                  )
                ],
              ),
              SizedBox(
                height: 62,
              ),
              SubHeadingText(
                subheading: "By continuing, you agree to our ",
                align: TextAlign.center,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SubHeadingText(
                    subheading: "Terms of Service",
                    color: kPrimaryColor,
                    fontWeight: kBoldSubHeading,
                  ),
                  SubHeadingText(
                    subheading: " & ",
                    fontWeight: kBoldSubHeading,
                  ),
                  SubHeadingText(
                    subheading: "Privacy",
                    color: kPrimaryColor,
                    fontWeight: kBoldSubHeading,
                  )
                ],
              )
            ],
          ),
        ),
      )),
    );
  }
}
