import 'package:flutter/material.dart';

final kSubheadingPrimary = Colors.grey[700];
final kBoldSubHeading = FontWeight.bold;
final kPrimaryColor = Color(0xff550097);
final kHeadingTextStyle = TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
final kHintTextStyle = TextStyle(fontSize: 14);
final rowTitleTextStyle = TextStyle(fontWeight: FontWeight.bold, fontSize: 25);
