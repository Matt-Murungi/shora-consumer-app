import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

final String assetName = 'assets/logo.svg';
final Widget logoSvg = SvgPicture.asset(
  assetName,
  semanticsLabel: 'Shora Logo',
  width: 360,
  height: 107,
);
