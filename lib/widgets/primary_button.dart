import 'package:flutter/material.dart';

class PrimaryButton extends StatelessWidget {
  final String label;
  final Color color;
  final Function function;

  PrimaryButton({this.label, this.color, this.function});

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
      padding: EdgeInsets.symmetric(vertical: 13),
      onPressed: function,
      color: color,
      child: Text(
        label,
        style: TextStyle(color: Colors.white),
      ),
    );
  }
}
