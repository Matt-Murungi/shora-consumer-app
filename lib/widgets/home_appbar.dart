import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:shora_consumer/styles.dart';
import 'package:shora_consumer/widgets/input.dart';
import 'package:shora_consumer/widgets/small_button.dart';

class HomeAppBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height / 5.5,
      decoration: BoxDecoration(
          color: Color(0xffF9F9F9),
          border: Border(bottom: BorderSide(width: 0.1))),
      child: Column(
        children: [
          ListTile(
            title: Text("You're shopping in"),
            subtitle: Text(
              "Gitega, Burundi",
              style: TextStyle(
                  color: kPrimaryColor,
                  fontWeight: FontWeight.bold,
                  fontSize: 17),
            ),
            trailing: Icon(
              Icons.shopping_cart_outlined,
              color: kPrimaryColor,
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 14),
            child: Input(
              hint: "Search for store, product and category",
              inputType: TextInputType.text,
              inputKey: "home search",
              leadWidget: Icon(Icons.search),
            ),
          ),
        ],
      ),
    );
  }
}

class RowTitle extends StatelessWidget {
  final String title;
  final String buttonTitle;
  RowTitle({this.title, this.buttonTitle});
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          title,
          style: rowTitleTextStyle,
        ),
        SmallButton(
          label: buttonTitle,
        ),
      ],
    );
  }
}
