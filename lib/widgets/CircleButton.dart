import 'package:flutter/material.dart';

import '../styles.dart';

class CircleButton extends StatelessWidget {

  final Function onPressed;
  CircleButton({this.onPressed});
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: CircleAvatar(
        backgroundColor: kPrimaryColor,
        // radius: 30,
        child: Icon(Icons.arrow_forward_ios_rounded) ,
      ),
    );
  }
}