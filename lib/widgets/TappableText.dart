import 'package:flutter/material.dart';
import 'package:shora_consumer/widgets/subheading.dart';

import '../styles.dart';

class TappableText extends StatelessWidget {
  final String text;
  final Function onPressed;
  TappableText({this.text, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      
      onTap: onPressed,
      child: SubHeadingText(
        subheading: text,
        color: kPrimaryColor,
        fontWeight: kBoldSubHeading,
      ),
    );
  }
}
