import 'package:flutter/material.dart';

class SmallButton extends StatelessWidget {
  final String label;
  final Function onPressed;
  SmallButton({this.label, this.onPressed});
  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: onPressed,
      child: Text(
        label,
        style: TextStyle(
            color: Colors.black, fontSize: 16, fontWeight: FontWeight.bold),
      ),
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.all(Colors.grey[300]),
      ),
    );
  }
}