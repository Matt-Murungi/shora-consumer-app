import 'package:flutter/material.dart';

class ImageCategory extends StatelessWidget {
  final String label;
  final String image;
  ImageCategory({this.label, this.image});
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(5)),
          child: Image.network(
            image,
            fit: BoxFit.contain,
            height: 100,
            width: 90,
          ),
        ),
        Text(
          label,
          textAlign: TextAlign.center,
        )
      ],
    );
  }
}
