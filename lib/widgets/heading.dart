import 'package:flutter/material.dart';

import '../styles.dart';

class HeadingText extends StatelessWidget {
  final String heading;

  HeadingText({this.heading});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 12),
      child: Text(
        heading,
        textAlign: TextAlign.center,
        style: kHeadingTextStyle,
      ),
    );
  }
}
