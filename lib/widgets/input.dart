import 'package:flutter/material.dart';

import '../styles.dart';

class Input extends StatelessWidget {
  final String hint;
  final TextInputType inputType;
  final Widget leadWidget;
  final Widget trailWidget;
  final TextEditingController controller;
  final String inputKey;
  // bool showPassword = false;

  Input(
      {@required this.hint,
      @required this.inputType,
      @required this.inputKey,
      this.leadWidget,
      this.trailWidget,
      this.controller});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      decoration: BoxDecoration(
        color: Color(0xffefefef),
        borderRadius: BorderRadius.all(
          Radius.circular(6),
        ),
      ),
      child: ListTile(
        leading: leadWidget,
        title: TextFormField(
          controller: controller,
          key: Key(inputKey),
          onChanged: (e) {},
          validator: (value) {
            if (value == null || value.isEmpty) {
              return "Please enter $hint";
            }
            return null;
          },
          keyboardType: inputType,
          obscureText:
              inputType == TextInputType.visiblePassword ? true : false,
          decoration: InputDecoration(
              border: InputBorder.none,
              hintText: hint,
              hintStyle: kHintTextStyle),
        ),
        trailing: trailWidget,
      ),
    );
  }
}
