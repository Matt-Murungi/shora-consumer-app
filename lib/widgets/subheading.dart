import 'package:flutter/material.dart';

class SubHeadingText extends StatelessWidget {
  final String subheading;
  final Color color;
  final FontWeight fontWeight;
  final TextAlign align;

  SubHeadingText({this.subheading, this.color, this.align, this.fontWeight});
  @override
  Widget build(BuildContext context) {
    return Text(
      subheading,
      textAlign: align,
      style: TextStyle(fontSize: 18, color: color, fontWeight: fontWeight),
    );
  }
}
