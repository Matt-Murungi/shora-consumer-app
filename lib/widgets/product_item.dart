import 'package:flutter/material.dart';

class ProductItem extends StatelessWidget {
  final String productTitle;
  final String productDescription;
  final String productPrice;
  final String productOldPrice;
  final String productImageUrl;
  ProductItem(
      {this.productTitle,
      this.productDescription,
      this.productPrice,
      this.productOldPrice,
      this.productImageUrl});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 16),
      child: Row(
        children: [
          Expanded(
            flex: 3,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  productTitle,
                  softWrap: true,
                  maxLines: 2,
                  style: TextStyle(fontSize: 20),
                ),
                SizedBox(
                  height: 6,
                ),
                Text(
                  productDescription,
                  style: TextStyle(fontSize: 16, color: Colors.grey),
                ),
                SizedBox(
                  height: 6,
                ),
                Row(
                  children: [
                    Text(
                      "$productPrice BIF",
                      style: TextStyle(fontSize: 18, color: Colors.black),
                    ),
                    SizedBox(
                      width: 15,
                    ),
                    Text(
                      productOldPrice.isNotEmpty ? "$productOldPrice BIF" : "",
                      style: TextStyle(
                          fontSize: 18,
                          decoration: TextDecoration.lineThrough,
                          color: Colors.grey),
                    ),
                  ],
                )
              ],
            ),
          ),
          Expanded(
            flex: 1,
            child: Image.network(
              productImageUrl,
              fit: BoxFit.cover,
            ),
          ),
        ],
        // padding: const EdgeInsets.symmetric(vertical: 16),
        // child: ListTile(
        //   title: Padding(
        //     padding: const EdgeInsets.only(bottom: 6),
        //     child: Text(
        //       productTitle,
        //       softWrap: true,
        //       maxLines: 2,
        //       style: TextStyle(fontSize: 18),
        //     ),
        //   ),
        //   subtitle: Column(
        //     crossAxisAlignment: CrossAxisAlignment.start,
        //     children: [
        //       Padding(
        //         padding: const EdgeInsets.symmetric(vertical: 6),
        //         child: Text(
        //           productDescription,
        //         ),
        //       ),
        //       Row(
        //         children: [
        //           Text(
        //             "$productPrice BIF",
        //             style: TextStyle(fontSize: 16, color: Colors.black),
        //           ),
        //           SizedBox(
        //             width: 10,
        //           ),
        //           Text(
        //             productOldPrice.isNotEmpty ? "$productOldPrice BIF" : "",
        //             style: TextStyle(
        //                 fontSize: 16, decoration: TextDecoration.lineThrough),
        //           ),
        //         ],
        //       )
        //     ],
        //   ),
        //   isThreeLine: true,

        //   trailing: Image.network(
        //     productImageUrl,
        //     fit: BoxFit.fill,
        //     height: 200,
        //     width: 100,
        //   ),
        // ),
      ),
    );
  }
}
