class Product {
  String title;
  String description;
  String oldPrice;
  String newPrice;
  String imageUrl;

  Product(
      {this.title,
      this.description,
      this.oldPrice,
      this.newPrice,
      this.imageUrl});
}
