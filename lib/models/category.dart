import 'package:flutter/foundation.dart';

class Category {
  String label;
  String image;

  Category({this.label, this.image});
}
