import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shora_consumer/core/home/home.dart';
import 'package:shora_consumer/core/login/login.dart';
import 'package:shora_consumer/core/register/register.dart';
import 'package:shora_consumer/core/shopping_location/shopping_location.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: LoginScreen(),
      getPages: [
        GetPage(name: '/login', page: () => LoginScreen()),
        GetPage(name: ('/register'), page: ()=> RegisterScreen()),
        GetPage(name: ('/home'), page: ()=> HomeScreen())
      ],
      title: "Shora",
    );
  }
}
