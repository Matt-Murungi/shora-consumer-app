import 'package:shora_consumer/models/products.dart';

List<Product> products = [
  Product(
      title: "Kona Brewing Co Big Wave Golden Ale",
      description: "(6x12 fl oz) Kona Brewing Company®…",
      oldPrice: "19,040 ",
      newPrice: "11,416 ",
      imageUrl:
          "https://res.cloudinary.com/testout/image/upload/v1629652223/Shora/Product/bitmap_xb1tpr.png"),
  Product(
      title: "Cigar City Brewing Jai Alai IPA",
      description: "(6x12 fl oz) 7.5% alcohol by volume",
      oldPrice: "20,946 ",
      newPrice: "16,753 ",
      imageUrl:
          "https://res.cloudinary.com/testout/image/upload/v1629652304/Shora/Product/bitmap_i6tstw.png"),
  Product(
      title: "Cruzan Black Strap Rum",
      description:
          "(750 ml) After the sugar makers have extracted everything they can from…",
      oldPrice: "20,946 ",
      newPrice: "16,753 ",
      imageUrl:
          "https://res.cloudinary.com/testout/image/upload/v1629652283/Shora/Product/bitmap_kpxkil.png"),
  Product(
      title: "Divina Olives, Castelvetrano",
      description:
          "(10.6 oz) Olives, Water, Salt, Lactic Acid (acidity Regulator)",
      oldPrice: "",
      newPrice: "5,698 ",
      imageUrl:
          "https://res.cloudinary.com/testout/image/upload/v1629653247/Shora/Product/bitmap_wux1nl.png"),
  Product(
      title: "Bartles & Jaymes Wine Cooler, Gluten-Free, Watermelon &…",
      description: "Carbonated Water, Grape Wine, Cane…",
      oldPrice: "",
      newPrice: "24,758 ",
      imageUrl:
          "https://res.cloudinary.com/testout/image/upload/v1629652252/Shora/Product/bitmap_dpd7ar.png"),
  Product(
      title: "Carlo Rossi Red Wine",
      description: "(3 L) Contains Sulfites",
      oldPrice: "20, 968 ",
      newPrice: "16,753 ",
      imageUrl:
          "https://res.cloudinary.com/testout/image/upload/v1629652242/Shora/Product/bitmap_nwdbut.png"),
  Product(
      title: "Vista Bay Coconut Mango Hard",
      description: "(3 L) Contains Sulfites",
      oldPrice: "20, 946",
      newPrice: "16,753 ",
      imageUrl:
          "https://res.cloudinary.com/testout/image/upload/v1629652233/Shora/Product/bitmap_yxf3su.png")
];
