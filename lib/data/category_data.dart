import 'package:shora_consumer/models/category.dart';

List<Category> categories = [
  Category(
      image:
          "https://res.cloudinary.com/testout/image/upload/v1628413249/Shora/Category/bitmap_copy_vl61k3.png",
      label: "Alcohol"),
  Category(
      image:
          "https://res.cloudinary.com/testout/image/upload/v1628413257/Shora/Category/bitmap_copy_vtyfbv.png",
      label: "Electronics"),
  Category(
      image:
          "https://res.cloudinary.com/testout/image/upload/v1628413277/Shora/Category/bitmap_copy_drtyjf.png",
      label: "Home"),
  Category(
      image:
          "https://res.cloudinary.com/testout/image/upload/v1628413286/Shora/Category/bitmap_copy_phqtac.png",
      label: "Grocerries"),
  Category(
      image:
          "https://res.cloudinary.com/testout/image/upload/v1628413303/Shora/Category/bitmap_copy_zk53ns.png",
      label: "HouseHold Essentials"),
  Category(
      image:
          "https://res.cloudinary.com/testout/image/upload/v1628413320/Shora/Category/bitmap_copy_fkrry0.png",
      label: "Women's")
];
